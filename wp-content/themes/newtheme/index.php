<?php get_header();

?>

<section>
    <div class="container">
        <div class="row inner-heading-sec">
            <div class="col-lg-12">
        <div class="theme-heading-sec text-center"><h2><?php echo get_the_title();?></h2></div>
    </div>
                  </div>

<?php if(have_posts()) { while(have_posts()) {
the_post();
the_content();
} } ?>
</div>
</section>



<?php get_footer();?>