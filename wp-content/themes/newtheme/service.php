<?php get_header();/* Template Name: service*/ ?>
<section class="service-banner">
     
     <div class="container">
        <div class="row">
        
         <link rel="stylesheet" href="theme.css">
    
 </div>
</div>
</section>
</br>

 <!--Services Two Start-->
        <section class="services-two">
            <div class="container">
                <div class="section-title text-center">
                    
                    <h2 class="section-title__title">The Services We’re Providing</h2>
                </div>
               
                <div class="row">
                        <div class="owl-carousel service-slide">
                     <?php $arg = array('post_type' =>'service', 'posts_per_page' => -1, 'order' => 'ASC');
                     $loop = new WP_query($arg);
                     if($loop->have_posts()){
                       while($loop->have_posts()){
                        $loop->the_post();                     

                    ?>
                
                         <div class="item ">
                        <!--Services Two Single-->
                        <div class="services-two__single">
                            <div class="services-two__content-box">
                                <div class="services-two__content-bg-box">
                                    <div class="services-two__content-bg"
                                        style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/services-two-content-bg.jpg);">
                                    </div>
                                </div>
                                <div class="services-two__icon">
                                    <?php the_post_thumbnail( 'full', array('class'=>'img-fluid'));?>
                                </div>
                                <div class="services-two__content">
                                    <h3 class="services-two__title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                                    <p class="services-two__text"><?php the_content();?></p>
                                </div>
                            </div>
                            <div class="services-two__read-more">
                                <a href="<?php the_permalink();?>">
                                    <span>Read More</span>
                                    <i class="fa fa-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                      <?php }  } ?>
                       </div>
                </div>
            </div>
        </section>
        <!--Services Two End-->

        <!--Welcome Start-->
        <section class="welcome">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="welcome__left">
                            <div class="welcome__img-box wow slideInLeft" data-wow-delay="100ms"
                                data-wow-duration="2500ms">
                                <div class="welcome__img">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/resources/welcome-img-1.jpg" alt="">
                                </div>
                                <div class="welcome__icon">
                                    <span class="icon-garbage"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="welcome__right">
                            <div class="section-title text-left">
                                <span class="section-title__tagline">Welcome to Waste Pickup Company</span>
                                <h2 class="section-title__title">We’ve Been Working Since 1987 in this Field</h2>
                            </div>
                            <p class="welcome__text-1">One of the major garbage pickup company</p>
                            <p class="welcome__text-2">We have the experience of 35 years and many more years to go.
                            </p>
                            <ul class="list-unstyled welcome__bottom">
                                <li>
                                    <div class="welcome__count"></div>
                                    <div class="welcome__content">
                                        <h3>Going Above <br> and Beyond</h3>
                                    </div>
                                </li>
                                <li>
                                    <div class="welcome__count"></div>
                                    <div class="welcome__content">
                                        <h3>committed to <br> People First</h3>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Welcome End-->

        <section>
             <div class="container">
                <div class="form-container">
                <div class="row">
                <div class="col-sm-11">
                    <div class="about-fth-sec">
                        <div class="about-fth-sec-ctnt">
                           <h3>Contact Form</h3>

<div class="container">
  <form action="/action_page.php">
    <label for="fname">First Name</label>
    <input type="text" id="fname" name="firstname" placeholder="Your name..">

    <label for="lname">Last Name</label>
    <input type="text" id="lname" name="lastname" placeholder="Your last name..">

    <label for="lname">Mail id</label>
    <input type="text" id="mail id" name="mail id" placeholder="Your mail id">

    <label for="country">Country</label>
    <select id="country" name="country">
      <option value="australia">Australia</option>
      <option value="canada">Canada</option>
      <option value="usa">USA</option>
      <option value="usa">India</option>
      <option value="usa">South Korea</option>
      <option value="usa">Indonesia</option>




    </select>

    <label for="subject">Message</label>
    <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>

    <input type="submit" value="Send Message">
  </form>
</div>
</div>
                        

                </div>
            </div>
         </section>

<?php get_footer();?>