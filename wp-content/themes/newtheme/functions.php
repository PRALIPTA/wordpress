<?php
require get_template_directory(). '/inc/theme-setting.php';
function theme_menu(){
  register_nav_menus (
    array(
    'primary_menu' => __('Header Menu'),
    'footer_menu' => __('Footer Menu'),
    'service_menu' => __('Service Menu')
    )
  );
}

add_action('init', 'theme_menu');
add_action( 'init', 'create_post_type' );
add_theme_support( 'post-thumbnails' );
function create_post_type() {
    register_post_type( 'service',
        array(
            'labels' => array(
                'name' => __( 'Services' ),
                'singular_name' => __( 'Services' )
            ),
        'public' => true,
        'supports' => array( 'title', 'editor', 'thumbnail'),
        'menu_icon'=>'dashicons-admin-tools',
        'has_archive' => true,
        )
    );
     register_post_type( 'projects',
        array(
            'labels' => array(
                'name' => __( 'Projects' ),
                'singular_name' => __( 'Projects' )
            ),
        'public' => true,
        'supports' => array( 'title', 'editor', 'thumbnail'),
        'menu_icon'=>'dashicons-media-default',
        'has_archive' => true,
        )
    );
     register_post_type( 'experts',
        array(
            'labels' => array(
                'name' => __( 'Experts' ),
                'singular_name' => __( 'Experts' )
            ),
        'public' => true,
        'supports' => array( 'title', 'editor', 'thumbnail'),
        'menu_icon'=>'dashicons-groups',
        'has_archive' => true,
        )
    );
    register_post_type( 'prices',
        array(
            'labels' => array(
                'name' => __( 'Prices' ),
                'singular_name' => __( 'Prices' )
            ),
        'public' => true,
        'supports' => array( 'title', 'editor', 'thumbnail'),
        'menu_icon'=>'dashicons-money-alt',
        'has_archive' => true,
        )
    ); 
    register_post_type( 'workingprocess',
        array(
            'labels' => array(
                'name' => __( 'Workingprocess' ),
                'singular_name' => __( 'Workingprocess' )
            ),
        'public' => true,
        'supports' => array( 'title', 'editor', 'thumbnail'),
        'menu_icon'=>'dashicons-hammer',
        'has_archive' => true,
        )
    );
    register_post_type( 'newsarticle',
        array(
            'labels' => array(
                'name' => __( 'Newsarticles' ),
                'singular_name' => __( 'Newsarticles' )
            ),
        'public' => true,
        'supports' => array( 'title', 'editor', 'thumbnail'),
        'menu_icon'=>'dashicons-media-text',
        'has_archive' => true,
        )
    );
     register_post_type( 'feedback',
        array(
            'labels' => array(
                'name' => __( 'Feedback' ),
                'singular_name' => __( 'Feedback' )
            ),
        'public' => true,
        'supports' => array( 'title', 'editor', 'thumbnail'),
        'menu_icon'=>'dashicons-welcome-write-blog',
        'has_archive' => true,
        )
    );

  }
  
   