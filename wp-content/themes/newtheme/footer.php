      <footer class="site-footer">
            <div class="site-footer-bg" style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/site-footer-bg.jpg);">
            </div>
            <div class="site-footer__top">
                <div class="container">
                    <div class="site-footer__top-inner">
                        <div class="site-footer__top-logo">
                            <a href="index.html"><img src="<?php bloginfo('template_directory'); ?>/assets/images/resources/footer-logo.png" alt=""></a>
                        </div>
                        <div class="site-footer__top-right">
                            <p class="site-footer__top-right-text">Waste Disposal Management & Pickup Services</p>
                            <div class="site-footer__social">
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-pinterest-p"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-footer__middle">
                <div class="container">
                    <div class="row">
                         
                        <div class="col-xl-4 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="100ms">
                            <div class="footer-widget__column footer-widget__about">
                               
                                <h3 class="footer-widget__title">About</h3>
                                <div class="footer-widget__about-text-box">
                                    <p class="footer-widget__about-text"><?php echo esc_attr( get_option('about-sec') ); ?></p>
                                </div>
                           
                                <form class="footer-widget__newsletter-form">
                                    <div class="footer-widget__newsletter-input-box">
                                        <input type="email" placeholder="Email Address" name="email">
                                        <button type="submit" class="footer-widget__newsletter-btn"><i
                                                class="far fa-paper-plane"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                         
                        <div class="col-xl-2 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="200ms">
                            <div class="footer-widget__column footer-widget__links clearfix">
                                <h3 class="footer-widget__title">Links</h3>

                                   <?php wp_nav_menu(array('theme_location'=>'footer_menu','menu_class'=>'footer-widget__links-list list-unstyled')) ?> 
                               
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="300ms">
                            <div class="footer-widget__column footer-widget__services clearfix">
                                <h3 class="footer-widget__title">services</h3>
                                <ul class="footer-widget__services-list list-unstyled clearfix">
                                   <!-- <li><a href="(<?php bloginfo('template_directory'); ?>dumpster-rental.html">Dumpster Rentals</a></li>
                                    <li><a href="(<?php bloginfo('template_directory'); ?>about.html">Bulk Trash Pickup</a></li>
                                    <li><a href="(<?php bloginfo('template_directory'); ?>about.html">Waste Removal</a></li>
                                    <li><a href="(<?php bloginfo('template_directory'); ?>zero-waste.html">Zero Waste</a></li>
                                </ul>-->
                                <?php wp_nav_menu(array('theme_location'=>'service_menu','menu_class'=>'footer-widget__links-list list-unstyled')) ?> 
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-6 wow fadeInUp" data-wow-delay="400ms">
                            <div class="footer-widget__column footer-widget__contact clearfix">
                                <h3 class="footer-widget__title">Contact</h3>
                                <p class="footer-widget__contact-text"><?php echo esc_attr( get_option('address-part') ); ?></p>
                                <div class="footer-widget__contact-info">
                                    <div class="footer-widget__contact-icon">
                                        <span class="icon-contact"><i class="fa fa-map-signs" aria-hidden="true"></i></span>
                                    </div>
                                    <div class="footer-widget__contact-content">
                                        <p class="footer-widget__contact-mail-phone">
                                            <a href="(<?php bloginfo('template_directory'); ?>mailto:needhelp@wostin.com"
                                                class="footer-widget__contact-mail"><?php echo esc_attr( get_option('java-email') ); ?></a>
                                            <a href="tel:2463330088" class="footer-widget__contact-phone"><?php echo esc_attr( get_option('phone-no') ); ?></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-footer__bottom">
                <div class="site-footer-bottom-shape"
                    style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/shapes/site-footer-bottom-shape.png);"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="site-footer__bottom-inner">
                                <p class="site-footer__bottom-text">© Copyright 2022 by <a href="http://ngodarpan.gov.in">NGOdarpan</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--Site Footer End-->


    </div><!-- /.page-wrapper -->


    <div class="mobile-nav__wrapper">
        <div class="mobile-nav__overlay mobile-nav__toggler"></div>
        <!-- /.mobile-nav__overlay -->
        <div class="mobile-nav__content">
            <span class="mobile-nav__close mobile-nav__toggler"><i class="fa fa-times"></i></span>

            <div class="logo-box">
                <a href="(<?php bloginfo('template_directory'); ?>index.html" aria-label="logo image"><img src="<?php bloginfo('template_directory'); ?>/assets/images/resources/footer-logo.png"
                        width="155" alt="" /></a>
            </div>
            <!-- /.logo-box -->
            <div class="mobile-nav__container"></div>
            <!-- /.mobile-nav__container -->

            <ul class="mobile-nav__contact list-unstyled">
                <li>
                    <i class="fa fa-envelope"></i>
                    <a href="(<?php bloginfo('template_directory'); ?>mailto:needhelp@packageName__.com">needhelp@wostin.com</a>
                </li>
                <li>
                    <i class="fa fa-phone-alt"></i>
                    <a href="(<?php bloginfo('template_directory'); ?>tel:666-888-0000">666 888 0000</a>
                </li>
            </ul><!-- /.mobile-nav__contact -->
            <div class="mobile-nav__top">
                <div class="mobile-nav__social">
                    <a href="#" class="fab fa-twitter"></a>
                    <a href="#" class="fab fa-facebook-square"></a>
                    <a href="#" class="fab fa-pinterest-p"></a>
                    <a href="#" class="fab fa-instagram"></a>
                </div><!-- /.mobile-nav__social -->
            </div><!-- /.mobile-nav__top -->



        </div>
        <!-- /.mobile-nav__content -->
    </div>
    <!-- /.mobile-nav__wrapper -->

    <div class="search-popup">
        <div class="search-popup__overlay search-toggler"></div>
        <!-- /.search-popup__overlay -->
        <div class="search-popup__content">
            <form action="#">
                <label for="search" class="sr-only">search here</label><!-- /.sr-only -->
                <input type="text" id="search" placeholder="Search Here..." />
                <button type="submit" aria-label="search submit" class="thm-btn">
                 <i class="fa fa-search" aria-hidden="true"></i>
                </button>
            </form>
        </div>
        <!-- /.search-popup__content -->
    </div>
    <!-- /.search-popup -->

    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/jquery/jquery-3.6.0.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/jarallax/jarallax.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/jquery-ajaxchimp/jquery.ajaxchimp.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/jquery-appear/jquery.appear.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/jquery-circle-progress/jquery.circle-progress.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/jquery-magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/jquery-validate/jquery.validate.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/nouislider/nouislider.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/odometer/odometer.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/swiper/swiper.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/tiny-slider/tiny-slider.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/wnumb/wNumb.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/wow/wow.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/isotope/isotope.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/countdown/countdown.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/bxslider/jquery.bxslider.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/vegas/vegas.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/jquery-ui/jquery-ui.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/vendors/timepicker/timePicker.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/wostin.js"></script>
    <script>
        $(".service-slide").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
        items: 1,
   responsive: {
          0: {
            items: 1
          },    

          375: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 3
          }
        }

  });
    </script>
    <?php wp_footer();?>
</body>

</html>