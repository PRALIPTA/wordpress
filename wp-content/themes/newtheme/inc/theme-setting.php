<?php
add_action('admin_menu', 'csutom_soc_add_create_menu');

function csutom_soc_add_create_menu() {

add_menu_page('Theme Option', 'option page', 'manage_options','social-address', 'my_custom_social_address_page','dashicons-align-pull-left'); 

//call register settings function
add_action( 'admin_init', 'register_my_custom_social_address' );
}


function register_my_custom_social_address() {
    
register_setting( 'custom-social-add', 'java-email' );
register_setting( 'custom-social-add', 'phone-no' );
register_setting( 'custom-social-add', 'about-sec' );
register_setting( 'custom-social-add', 'address-part' );
register_setting( 'custom-social-add', 'facebook-part' );
register_setting( 'custom-social-add', 'instagram-part' );
}

function my_custom_social_address_page() {
?>

<div class="wrap">
    <div style="background: #c1c1c1; padding: 7px; width: 80%; margin:30px 3px 10px;">
        <h2 style="line-height: 1; margin: 0; font-size: 22px;">Social links and Address</h2>
    </div>
    <form method="post" action="options.php">
    <style>
    .form-table td {
        margin-bottom: 3px;
    padding: 6px 10px;}
    </style>
        <?php settings_fields( 'custom-social-add' ); ?>
        <?php do_settings_sections( 'custom-social-add' ); ?>
        <table class="form-table">
            <tr valign="top">
                             <td>
            <label>Email:</label><br>
            <input type="email" name="java-email" value="<?php echo esc_attr( get_option('java-email') ); ?>" class="regular-text" style="height: 36px; margin-top: 10px;" /></td>
            <td>
                <label>Phone no:</label><br>
            <input type="number" name="phone-no" value="<?php echo esc_attr( get_option('phone-no') ); ?>" class="regular-text" style="height: 36px; margin-top: 10px;" /></td>
              
        </tr>
        <tr><td colspan="2">
            <label>address:</label><br>
            <textarea name="address-part" style="width:82%; margin-top:10px; height: 60px;"><?php echo esc_attr( get_option('address-part') ); ?></textarea></td></tr>
        <tr>
             <td colspan="2">
            <label>about:</label><br>
            <textarea name="about-sec" style="width:82%; margin-top:10px; height: 60px;"><?php echo esc_attr( get_option('about-sec') ); ?></textarea></td>
        </tr>
            <tr>
                             <td>
            <label>facebook</label><br>
            <input type="url" name="facebook-part" value="<?php echo esc_attr( get_option('facebook-part') ); ?>" class="regular-text" style="height: 36px; margin-top: 10px;" /></td>
            <td>
                <label>instagram</label><br>
            <input type="url" name="instagram-part" value="<?php echo esc_attr( get_option('instagram-part') ); ?>" class="regular-text" style="height: 36px; margin-top: 10px;" /></td>
              
        </tr>

        </table>      
    <?php submit_button();?>
    </form>
</div>
<?php }