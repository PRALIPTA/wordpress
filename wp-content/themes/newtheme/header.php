<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_directory'); ?>/assets/images/favicons/apple-touch-icon.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory'); ?>/assets/images/favicons/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory'); ?>/assets/images/favicons/favicon-16x16.png" />
    <link rel="manifest" href="<?php bloginfo('template_directory'); ?>/assets/images/favicons/site.webmanifest" />
    <link
        href="<?php bloginfo('template_directory'); ?>https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap"
        rel="stylesheet">
    <link href="<?php bloginfo('template_directory'); ?>https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/assets/vendors/animate/animate.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/animate/custom-animate.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/fontawesome/css/all.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/jarallax/jarallax.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/jquery-magnific-popup/jquery.magnific-popup.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/nouislider/nouislider.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/nouislider/nouislider.pips.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/odometer/odometer.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/swiper/swiper.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/tiny-slider/tiny-slider.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/reey-font/stylesheet.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/owl-carousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/owl-carousel/owl.theme.default.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/bxslider/jquery.bxslider.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/bootstrap-select/css/bootstrap-select.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/vegas/vegas.min.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/jquery-ui/jquery-ui.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/vendors/timepicker/timePicker.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/style.css" />
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/responsive.css" />
    <?php wp_head();?>
</head>
<body>
    <div class="preloader">
        <img class="preloader__image" width="60" src="(<?php bloginfo('template_directory'); ?>/assets/images/loader.png" alt="" />
    </div>
    <!-- /.preloader -->
    <div class="page-wrapper">
       
        <header class="main-header-three">
            <nav class="main-menu main-menu-three">
                <div class="main-menu-three__wrapper">
                    <div class="main-menu-wrapper__logo">
                        <a href="<?php echo home_url();?>"><img src="<?php bloginfo('template_directory'); ?>/assets/images/resources/logo-2.png" alt=""></a>
                    </div>
                    <div class="main-menu-three__main-menu">
                        <a href="#" class="mobile-nav__toggler"><i class="fa fa-bars"></i></a>
                       <?php wp_nav_menu(array('theme_location'=>'primary_menu','menu_class'=>'main-menu__list')) ?> 
                    </div>
                    <div class="main-menu-three__right">
                        <div class="main-menu-three__search-box">
                            <a href="#" class="main-menu-three__search search-toggler"><i class="fa fa-search" aria-hidden="true"></i></a>
                        </div>
                        <div class="main-menu-three__call">
                            <div class="main-menu-three__call-icon">
                                <span class="icon-phone-ringing"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            </div>
                            <div class="main-menu-three__call-number">
                                <p>Have Waste/Pickup?</p>
                                <h5><a href="tel:12463330088">+ 1- (246) 333-0088</a></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>