<?php get_header();?>


  <section class="main-slider main-slider-three">
            <div class="swiper-container thm-swiper__slider" data-swiper-options='{"slidesPerView": 1, "loop": false,
                "effect": "fade",
                "pagination": {
                "el": "#main-slider-pagination",
                "type": "bullets",
                "clickable": true
                },
                "navigation": {
                "nextEl": "#main-slider__swiper-button-next",
                "prevEl": "#main-slider__swiper-button-prev"
                },
                "autoplay": {
                "delay": 5000
                }}'>
                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <div class="main-slider-three-bg"
                            style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/main-slider-3-bg.jpg);">
                        </div>
                        <div class="main-slider-three-bg-two"></div>
                        <div class="main-slider-three-building"><img
                                src="<?php bloginfo('template_directory'); ?>/assets/images/resources/main-slider-three-building.png" alt=""></div>
                        <div class="main-slider-three-car"><img src="<?php bloginfo('template_directory'); ?>/assets/images/resources/main-slider-three-car.png"
                                alt="" class="float-bob-y"></div>
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-7">
                                    <div class="main-slider__content">
                                        <h2>Reliable <br> & Affordable <br> Waste Services</h2>
                                        <a href="<?php echo home_url('contact');?>" class="thm-btn">Request a Pickup</a>
                                        <a href="<?php echo home_url('contact');?>" class="thm-btn">scrap sell</a>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- If we need navigation buttons -->

            </div>
        </section>
          <!--Brand One Start-->
        <section class="brand-one brand-three">
            <div class="container">
                <div class="thm-swiper__slider swiper-container" data-swiper-options='{"spaceBetween": 100, "slidesPerView": 5, "autoplay": { "delay": 5000 }, "breakpoints": {
                    "0": {
                        "spaceBetween": 30,
                        "slidesPerView": 2
                    },
                    "375": {
                        "spaceBetween": 30,
                        "slidesPerView": 2
                    },
                    "575": {
                        "spaceBetween": 30,
                        "slidesPerView": 3
                    },
                    "767": {
                        "spaceBetween": 50,
                        "slidesPerView": 4
                    },
                    "991": {
                        "spaceBetween": 50,
                        "slidesPerView": 5
                    },
                    "1199": {
                        "spaceBetween": 100,
                        "slidesPerView": 5
                    }
                }}'>
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-1.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-2.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-3.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-4.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-5.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-1.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-2.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-3.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-4.png" alt="">
                        </div><!-- /.swiper-slide -->
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-5.png" alt="">
                        </div><!-- /.swiper-slide -->
                    </div>
                </div>
            </div>
        </section>
        <!--Brand One End-->

        <!--Services Two Start-->
        <section class="services-two">
            <div class="container">
                <div class="section-title text-center">
                    <span class="section-title__tagline">What We’re Offering</span>
                    <h2 class="section-title__title">The Services We’re Providing</h2>
                </div>
               
                <div class="row">
                        <div class="owl-carousel service-slide">
                     <?php $arg = array('post_type' =>'service', 'posts_per_page' => 3, 'order' => 'ASC');
                     $loop = new WP_query($arg);
                     if($loop->have_posts()){
                       while($loop->have_posts()){
                        $loop->the_post();                     

                    ?>
                
                         <div class="item ">
                        <!--Services Two Single-->
                        <div class="services-two__single">
                            <div class="services-two__content-box">
                                <div class="services-two__content-bg-box">
                                    <div class="services-two__content-bg"
                                        style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/services-two-content-bg.jpg);">
                                    </div>
                                </div>
                                <div class="services-two__icon">
                                    <?php the_post_thumbnail( 'full', array('class'=>'img-fluid'));?>
                                </div>
                                <div class="services-two__content">
                                    <h3 class="services-two__title"><?php the_title();?></a></h3>
                                    <p class="services-two__text"></p>
                                </div>
                            </div>
                            <div class="services-two__read-more">
                                <a href="<?php echo get_the_permalink();?>">
                                    <span>Read More</span>
                                    <i class="fa fa-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                      <?php }  } ?>
                       </div>
                </div>
            </div>
        </section>
        <!--Services Two End-->

        <!--Welcome Start-->
        <section class="welcome">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="welcome__left">
                            <div class="welcome__img-box wow slideInLeft" data-wow-delay="100ms"
                                data-wow-duration="2500ms">
                                <div class="welcome__img">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/resources/welcome-img-1.jpg" alt="">
                                </div>
                                <div class="welcome__icon">
                                    <span class="icon-garbage"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="welcome__right">
                            <div class="section-title text-left">
                                <span class="section-title__tagline">Welcome to Waste Pickup Company</span>
                                <h2 class="section-title__title">We’ve Been Working Since 1987 in this Field</h2>
                            </div>
                            <p class="welcome__text-1">One of the major garbage pickup company</p>
                            <p class="welcome__text-2">We have the experience of 35 years and many more years to go.
                            </p>
                            <ul class="list-unstyled welcome__bottom">
                                <li>
                                    <div class="welcome__count"></div>
                                    <div class="welcome__content">
                                        <h3>Going Above <br> and Beyond</h3>
                                    </div>
                                </li>
                                <li>
                                    <div class="welcome__count"></div>
                                    <div class="welcome__content">
                                        <h3>committed to <br> People First</h3>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Welcome End-->

        <!--Why Choose Two Start-->
        <section class="why-choose-two">
            <div class="why-choose-two-bg"
                style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/why-choose-two-bg.jpg);"></div>
            <div class="container">
                <div class="row">

                    <div class="col-xl-6 col-lg-6">
                        <div class="why-choose-two__left">
                            <div class="section-title text-left">
                                <span class="section-title__tagline">Why Choose Us?</span>
                                <h2 class="section-title__title">We Make Sure your Waste Goes to the Right Place</h2>
                            </div>
                            <!--<p class="why-choose-two__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enim ad minim veniam.
                            </p>-->
                            <div class="why-choose-two__founder">
                                <div class="why-choose-two__founder-img">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/resources/why-choose-two-founder-img.jpg" alt="">
                                </div>
                                <div class="why-choose-two__founder-content">
                                    <h3 class="why-choose-two__founder-name">Kevin Martin</h3>
                                    <p class="why-choose-two__founder-title">CEO - Co Founder</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="why-choose-two__right">
                            <ul class="list-unstyled why-choose-two__counter">
                                <li class="why-choose-two__counter-single wow fadeInUp" data-wow-delay="100ms">
                                    <div class="why-choose-two__counter-icon">
                                        <span class="icon-waste"></span>
                                    </div>
                                    <div class="why-choose-two__counter-content">
                                        <h2 class="odometer" data-count="4850">00</h2><span
                                            class="counter-two__plus">+</span>
                                        <p class="why-choose-two__counter-text">Waste Picked & Dispose </p>
                                    </div>
                                </li>
                                <li class="why-choose-two__counter-single wow fadeInUp" data-wow-delay="200ms">
                                    <div class="why-choose-two__counter-icon">
                                        <span class="icon-success"></span>
                                    </div>
                                    <div class="why-choose-two__counter-content">
                                        <h2 class="odometer" data-count="99.9">00</h2><span
                                            class="counter-two__plus">%</span>
                                        <p class="why-choose-two__counter-text">Our Company is Successful</p>
                                    </div>
                                </li>
                                <li class="why-choose-two__counter-single wow fadeInUp" data-wow-delay="300ms">
                                    <div class="why-choose-two__counter-icon">
                                        <span class="icon-affection"></span>
                                    </div>
                                    <div class="why-choose-two__counter-content">
                                        <h2 class="odometer" data-count="20660">00</h2><span
                                            class="counter-two__plus">+</span>
                                        <p class="why-choose-two__counter-text">Satisfied & Happy People</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Why Choose Two End-->

        <!--Manage Waste Two Start-->
        <section class="manage-waste manage-waste-two">
            <div class="manage-waste-bg-box">
                <div class="manage-waste-bg jarallax" data-jarallax data-speed="0.2" data-imgPosition="50% 0%"
                    style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/manage-waste-bg.jpg);"></div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="manage-waste__inner">
                            <h3 class="manage-waste__title">Manage Waste Effectively <br> and Reduce Environmental
                                Impact</h3>
                            <div class="manage-waste__btn-box">
                                <a href="<?php echo home_url('contact');?>" class="thm-btn manage-waste__btn-1">Request a Pickup</a>
                                <a href="<?php echo home_url('contact');?>" class="thm-btn manage-waste__btn-2">Contact With us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Manage Waste Two End-->

        <!--Project Three Start-->
        <section class="project-three">
            <div class="container">
                <div class="project-three__top">
                    <div class="row">
                        <div class="col-xl-7 col-lg-7">
                            <div class="project-three__left">
                                <div class="section-title text-left">
                                    <span class="section-title__tagline">Recent Closed Projects</span>
                                    <h2 class="section-title__title">Recently Completed Projects</h2>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <div class="project-three__bottom">
                    <div class="row">
                        <?php $arg = array('post_type' =>'projects', 'posts_per_page' => 3, 'order' => 'ASC');
                     $loop = new WP_query($arg);
                     if($loop->have_posts()){
                       while($loop->have_posts()){
                        $loop->the_post();                     

                    ?>  
                        <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="100ms">
                            <!--Project Three Single-->
                           
                            <div class="project-three__single">
                                <div class="project-three__img">

                                    <!--<img src="<?php bloginfo('template_directory'); ?>/assets/images/resources/project-3-1.jpg" alt="">-->
                                    <?php the_post_thumbnail( 'full', array('class'=>'img-fluid'));?>
                                    <div class="project-three__content">
                                        <div class="project-three__arrow">
                                            <a href="<?php echo get_the_permalink();?>"><i class="fa fa-arrow-right"></i></a>
                                        </div>
                                        <p class="project-three__subtitle"><?php the_title();?></p>
                                        <h3 class="project-three__title"><a href="<?php the_title();?>"></a></h3>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    <?php } }?>
                       <!-- <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="200ms">
                           
                            <div class="project-three__single">
                                <div class="project-three__img">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/resources/project-3-2.jpg" alt="">
                                    <div class="project-three__content">
                                        <div class="project-three__arrow">
                                            <a href="project-details.html"><i class="fa fa-arrow-right"></i></a>
                                        </div>
                                        <p class="project-three__subtitle">Waste Pickup</p>
                                        <h3 class="project-three__title"><a href="project-details.html">Home Trash
                                                Picked</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="300ms">
                            
                            <div class="project-three__single">
                                <div class="project-three__img">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/resources/project-3-3.jpg" alt="">
                                    <div class="project-three__content">
                                        <div class="project-three__arrow">
                                            <a href="project-details.html"><i class="fa fa-arrow-right"></i></a>
                                        </div>
                                        <p class="project-three__subtitle">Waste Pickup</p>
                                        <h3 class="project-three__title"><a href="project-details.html">Construction
                                                Collection</a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->
               
                </div>
            </div>
        </section>
        <!--Project Three End-->

        <!--Testimonial Three Start-->
        <section class="testimonial-three">
            <div class="testimonial-three-bg"
                style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/testimonial-three-bg.jpg);"></div>
            <div class="testimonial-three-bg-2"
                style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/testimonial-three-bg-2.png);"></div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-8">
                        <div class="testimonial-three__left">
                            <div class="section-title text-left">
                                <span class="section-title__tagline">Our Customers Feedbacks</span>
                                <h2 class="section-title__title">What out customer telling about us.</h2>
                            </div>
                            <div class="testimonial-three__carousel owl-theme owl-carousel thm-owl__carousel"
                                data-owl-options='{
                                "loop": true,
                                "autoplay": true,
                                "margin": 30,
                                "nav": false,
                                "dots": false,
                                "smartSpeed": 500,
                                "autoplayTimeout": 10000,
                                "navText": ["<span class=\"fa fa-angle-left\"></span>","<span class=\"fa fa-angle-right\"></span>"],
                                "responsive": {
                                    "0": {
                                        "items": 1
                                    },
                                    "768": {
                                        "items": 1
                                    },
                                    "1200": {
                                        "items": 1
                                    }
                                }
                            }'>
                                <!--Testimonial One Single-->
                                <?php $arg = array('post_type' =>'feedback', 'posts_per_page' => 3, 'order' => 'ASC');
                            $loop = new WP_query($arg);
                           if($loop->have_posts()){
                           while($loop->have_posts()){
                               $loop->the_post();                     

                                 ?>
                                <div class="testimonial-three__single">
                                    <div class="testimonial-three__feedback-box">
                                        <div class="testimonial-three__feedback">
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                        </div>
                                        <h3 class="testimonial-three__title">great job and fair pricing</h3>
                                    </div>
                                    <p class="testimonial-three__text"><?php the_content()?></p>
                                    <div class="testimonial-three__client-info-box">
                                        <div class="testimonial-three__client-info">
                                            <div class="testimonial-three__client-img">
                                                <!--<img src="<?php bloginfo('template_directory'); ?>/assets/images/testimonial/testimonial-3-1.jpg" alt="">-->
                                                <?php the_post_thumbnail( 'full', array('class'=>'img-fluid'));?>
                                            </div>
                                            <div class="testimonial-three__client-content">
                                                <h4 class="testimonial-three__client-name"><?php the_title();?></h4>
                                                <p class="testimonial-three__client-title">Customer</p>
                                            </div>
                                        </div>
                                        <div class="testimonial-three__quote">
                                            <span class="icon-quote"></span>
                                        </div>
                                    </div>
                                </div>
                            <?php } }?>
                                <!--Testimonial One Single-->
                               <!-- <div class="testimonial-three__single">
                                    <div class="testimonial-three__feedback-box">
                                        <div class="testimonial-three__feedback">
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                        </div>
                                        <h3 class="testimonial-three__title">Great job and fair pricing</h3>
                                    </div>
                                    <p class="testimonial-three__text">Proin a lacus arcu. Nullam id dui eu orci
                                        maximus. Cras at auctor lectus, vel pretium tellus. Class aptent sociosqu ad
                                        litora torquent per conubia nostra.</p>
                                    <div class="testimonial-three__client-info-box">
                                        <div class="testimonial-three__client-info">
                                            <div class="testimonial-three__client-img">
                                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/testimonial/testimonial-3-2.jpg" alt="">
                                            </div>
                                            <div class="testimonial-three__client-content">
                                                <h4 class="testimonial-three__client-name">Kevin Martin</h4>
                                                <p class="testimonial-three__client-title">Customer</p>
                                            </div>
                                        </div>
                                        <div class="testimonial-three__quote">
                                            <span class="icon-quote"></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="testimonial-three__single">
                                    <div class="testimonial-three__feedback-box">
                                        <div class="testimonial-three__feedback">
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                            <a href="#"><i class="fa fa-star"></i></a>
                                        </div>
                                        <h3 class="testimonial-three__title">Great job and fair pricing</h3>
                                    </div>
                                    <p class="testimonial-three__text">Proin a lacus arcu. Nullam id dui eu orci
                                        maximus. Cras at auctor lectus, vel pretium tellus. Class aptent sociosqu ad
                                        litora torquent per conubia nostra.</p>
                                    <div class="testimonial-three__client-info-box">
                                        <div class="testimonial-three__client-info">
                                            <div class="testimonial-three__client-img">
                                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/testimonial/testimonial-3-3.jpg" alt="">
                                            </div>
                                            <div class="testimonial-three__client-content">
                                                <h4 class="testimonial-three__client-name">Mike Hardson</h4>
                                                <p class="testimonial-three__client-title">Customer</p>
                                            </div>
                                        </div>
                                        <div class="testimonial-three__quote">
                                            <span class="icon-quote"></span>
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Testimonial Three End-->      

        <!--Team One Start-->
        <section class="team-one">
            <div class="container">
                <div class="section-title text-center">
                    <span class="section-title__tagline">Team Behind Success</span>
                    <h2 class="section-title__title">Expert Staff & Members</h2>
                </div>
                <div class="row">
                    <div class="row">
                     <?php $arg = array('post_type' =>'experts', 'posts_per_page' => 3, 'order' => 'ASC');
                     $loop = new WP_query($arg);
                     if($loop->have_posts()){
                       while($loop->have_posts()){
                        $loop->the_post();                     

                    ?>
                    <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="100ms">
                        <!--Team One Single-->
                        <div class="team-one__sngle">
                            <div class="team-one__img">
                               <?php the_post_thumbnail( 'full', array('class'=>'img-fluid'));?> 
                                <div class="team-one__shape">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/team-one-shape.png" alt="">
                                </div>
                            </div>
                            <div class="team-one__content">
                                <p class="team-one__title">Accountant</p>
                                <h3 class="team-one__name"><?php the_title();?></h3>
                                <div class="team-one__social-box">
                                    <div class="team-one__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                    </div>
                                    <div class="team-one__arrow">
                                        <a href="staff-details.html"><i class="fa fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   <!-- <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="200ms">
                       
                        <div class="team-one__sngle">
                            <div class="team-one__img">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/team/team-1-2.jpg" alt="">
                                <div class="team-one__shape">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/team-one-shape.png" alt="">
                                </div>
                            </div>
                            <div class="team-one__content">
                                <p class="team-one__title">Co Founder</p>
                                <h3 class="team-one__name">Jessica Brown</h3>
                                <div class="team-one__social-box">
                                    <div class="team-one__social">
                                       <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                    </div>
                                    <div class="team-one__arrow">
                                        <a href="<?php bloginfo('template_directory'); ?>staff-details.html"><i class="fa fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 wow fadeInUp" data-wow-delay="300ms">
                       
                        <div class="team-one__sngle">
                            <div class="team-one__img">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/team/team-1-3.jpg" alt="">
                                <div class="team-one__shape">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/team-one-shape.png" alt="">
                                </div>
                            </div>
                            <div class="team-one__content">
                                <p class="team-one__title">Senior Assistant</p>
                                <h3 class="team-one__name">David Cooper</h3>
                                <div class="team-one__social-box">
                                    <div class="team-one__social">
                                     <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                    </div>
                                    <div class="team-one__arrow">
                                        <a href="staff-details.html"><i class="fa fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <?php }  } ?>
                </div>
            </div>
        </section>
        <!--Team One End-->
<!--Working Process Start-->
        <section class="working-process">
            <div class="container">
                <div class="section-title text-center">
                    <span class="section-title__tagline">4 Simple Steps</span>
                    <h2 class="section-title__title">Our Working Process</h2>
                </div>
                 
                <ul class="list-unstyled working-process__box">
                    <?php $arg = array('post_type' =>'workingprocess', 'posts_per_page' => 4, 'order' => 'ASC');
                     $loop = new WP_query($arg);
                     if($loop->have_posts()){
                       while($loop->have_posts()){
                        $loop->the_post();                     

                    ?>
                    <li class="working-process__single">
                        <div class="working-process__icon">
                            <?php the_post_thumbnail( 'full', array('class'=>'img-fluid'));?> 
                           <!-- <span class="icon-garbage-sec"><img src="<?php bloginfo('template_directory'); ?>/assets/images/garbage-truck.png" class="img-fluid"></span>-->
                            <div class="working-process__count"></div>
                            <div class="working-process__shape">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/working-process-shape.png" alt="">
                            </div>
                            <div class="working-process__-hover-shape">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/working-process-hover-shape.png" alt="">
                                

                            </div>
                            <div class="working-process__bg-box">
                                <div class="working-process__bg"
                                    style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/working-process-bg.jpg);">


                                </div>
                            </div>
                        </div>
                        <h3 class="working-process__title"><a href="about.html"><?php the_title();?></a></h3>
                        <p class="working-process__text"><?php the_content();?>
                        </p>
                    </li>
                   <!-- <li class="working-process__single">
                        <div class="working-process__icon">
                            <span class="icon-garbage-sec"><img src="<?php bloginfo('template_directory'); ?>/assets/images/garbage.png" class="img-fluid"></span>
                            <div class="working-process__count"></div>
                            <div class="working-process__shape">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/working-process-shape.png" alt="">
                            </div>
                            <div class="working-process__-hover-shape">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/working-process-hover-shape.png" alt="">
                            </div>
                            <div class="working-process__bg-box">
                                <div class="working-process__bg"
                                    style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/working-process-bg.jpg);">
                                </div>
                            </div>
                        </div>
                        <h3 class="working-process__title"><a href="about.html">Collection</a></h3>
                        <p class="working-process__text">Lorem Ipsum is simply text of the new design print and type.
                        </p>
                    </li>
                    <li class="working-process__single">
                        <div class="working-process__icon">
                             <span class="icon-garbage-sec"><img src="<?php bloginfo('template_directory'); ?>/assets/images/waste.png" class="img-fluid"></span>
                            <div class="working-process__count"></div>
                            <div class="working-process__shape">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/working-process-shape.png" alt="">
                            </div>
                            <div class="working-process__-hover-shape">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/working-process-hover-shape.png" alt="">
                            </div>
                            <div class="working-process__bg-box">
                                <div class="working-process__bg"
                                    style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/working-process-bg.jpg);">
                                </div>
                            </div>
                        </div>
                        <h3 class="working-process__title"><a href="about.html">Proccessing</a></h3>
                        <p class="working-process__text">Lorem Ipsum is simply text of the new design print and type.
                        </p>
                    </li>
                    <li class="working-process__single">
                        <div class="working-process__icon">
                            <span class="icon-garbage-sec"><img src="<?php bloginfo('template_directory'); ?>/assets/images/waste-trc.png" class="img-fluid"></span>
                            <div class="working-process__count"></div>
                            <div class="working-process__shape">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/working-process-shape.png" alt="">
                            </div>
                            <div class="working-process__-hover-shape">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/shapes/working-process-hover-shape.png" alt="">
                            </div>
                            <div class="working-process__bg-box">
                                <div class="working-process__bg"
                                    style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/working-process-bg.jpg);">
                                </div>
                            </div>
                        </div>
                        <h3 class="working-process__title"><a href="about.html">Disposed</a></h3>
                        <p class="working-process__text">Lorem Ipsum is simply text of the new design print and type.
                        </p>
                    </li>-->
                <?php } }?>
                </ul>
            </div>
        </section>
        <!--Working Process End-->

        <!--News Three Start-->
        <section class="news-three">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-4">
                        <div class="news-three__left">
                            <div class="news-three__bg-box">
                                <div class="news-three__bg"
                                    style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/images/backgrounds/news-three-bg.jpg);"></div>
                            </div>
                            <div class="section-title text-left">
                                <span class="section-title__tagline">What’s Happening</span>
                                <h2 class="section-title__title">Our Latest News & Articles</h2>
                            </div>
                            <p class="news-three__text">some articles about our company</p>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-8">
                        <div class="news-three__right">
                            <div class="news-three__carousel owl-carousel owl-theme thm-owl__carousel" data-owl-options='{
                                "loop": true,
                                "autoplay": true,
                                "margin":30,
                                "nav": true,
                                "dots": false,
                                "smartSpeed": 500,
                                "autoplayTimeout": 10000,
                                "navText": ["<span class=\"fa fa-long-arrow-right\"></span>","<span class=\"fa fa-long-arrow-right\"></span>"],
                                "responsive": {
                                    "0": {
                                        "items": 1
                                    },
                                    "768": {
                                        "items": 1
                                    },
                                    "992": {
                                        "items": 2
                                    },
                                    "1200": {
                                        "items": 2.01
                                    }
                                }
                            }'>
                                <!--News One Single-->
                                  <?php $arg = array('post_type' =>'newsarticle', 'posts_per_page' => 3, 'order' => 'ASC');
                     $loop = new WP_query($arg);
                     if($loop->have_posts()){
                       while($loop->have_posts()){
                        $loop->the_post();                     

                    ?>
                                <div class="news-one__single">
                                    <div class="news-one__img">                                     
                                         <?php the_post_thumbnail( 'full', array('class'=>'img-fluid'));?> 
                                        <div class="news-one__date">
                                            <p>20 Dec</p>
                                        </div>
                                        <a href="news-details.html">
                                            <span class="news-one__plus"></span>
                                        </a>
                                    </div>
                                    <div class="news-one__content">
                                        <ul class="list-unstyled news-one__meta">
                                            <li><a href="news-details.html"><i class="far fa-clock"></i> by Admin </a>
                                            </li>
                                            <li><span>/</span></li>
                                            <li><a href="news-details.html"><i class="far fa-comments"></i> 2
                                                    Comments</a>
                                            </li>
                                        </ul>
                                        <h3 class="news-one__title"><a href="news-details.html"><?php the_content();?></a></h3>
                                        <div class="news-one__read-more">
                                            <a href="<?php echo get_the_permalink();?>"><i class="fa fa-arrow-right"></i>Read More</a>
                                        </div>
                                    </div>
                                </div>
                               <?php } }?>
                                
                            </div>                             
                        </div>                        
                    </div>
                        
                   
                </div>
                 
            </div>
            
        </section>

        <!--News Three End-->

        <!--Brand One Start-->
       <!-- <section class="brand-one brand-three">
            <div class="container">
                <div class="thm-swiper__slider swiper-container" data-swiper-options='{"spaceBetween": 100, "slidesPerView": 5, "autoplay": { "delay": 5000 }, "breakpoints": {
                    "0": {
                        "spaceBetween": 30,
                        "slidesPerView": 2
                    },
                    "375": {
                        "spaceBetween": 30,
                        "slidesPerView": 2
                    },
                    "575": {
                        "spaceBetween": 30,
                        "slidesPerView": 3
                    },
                    "767": {
                        "spaceBetween": 50,
                        "slidesPerView": 4
                    },
                    "991": {
                        "spaceBetween": 50,
                        "slidesPerView": 5
                    },
                    "1199": {
                        "spaceBetween": 100,
                        "slidesPerView": 5
                    }
                }}'>
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-1.png" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-2.png" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-3.png" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-4.png" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-5.png" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-1.png" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-2.png" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-3.png" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-4.png" alt="">
                        </div>
                        <div class="swiper-slide">
                            <img src="<?php bloginfo('template_directory'); ?>/assets/images/brand/brand-1-5.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
        <!--Brand One End-->

<?php get_footer();?>
        