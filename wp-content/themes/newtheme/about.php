<?php get_header();/* Template Name: about */ ?>
 

   <section class="inner-banner">
     
    <div class="container">
        <div class="row">
        
         <link rel="stylesheet" href="theme.css">
     
     </div>
    </div>
</section>

<section>
     
    <div class="container">
        <div class="row"> 
     <h1 class="">About us</h1></br>
     <p> The Kabadiwala.com (Start-up India Recognized) is a product of Asar Green Kabadi Pvt. Ltd. A technology-driven company working on SaaS (Software as a Service) based model to organize the waste management sector.</p>
     </div>
    </div>
</section>
  </br>
<section>
    <div class="container">
        <div class="row">
        <h1> Founders message</h1>
        <p style="color:green"><center><b>“Waste is never a waste, it’s rather an opportunity to something new”</b></center></p></br>
        <p>Mr. Anurag Asati, who co-founded The Kabadiwala with Kavindra Raghuwanshi in 2014 always wanted to do something out of the box but he had never anticipated something as bizarre as “waste” would drive him to bring the solution until he detected the biggest loopholes associated with waste management sector by then.
The unorganized sector made it a task for him to sell his own household scrap, it is then that he decided to infuse technology into the sector making it easy for thousands of consumers to sell scrap online.
But, this wasn’t enough to mark a significant impact, on households, The Kabadiwala gradually started catering to small to big businesses, corporates to institutions with their Extended Producer Responsibility (EPR), assisted the state government in managing the city’s huge dumps at The material recovery facility (MRF Centers) through involving waste workers from the informal sector, helping them earn the wage they deserve. Today, The Kabadiwala has become a one-stop solution to every waste chaos.</p>        
        </div>
    </div>
</section>

<section>
    <div class="container">
         <h1>Our core values</h1>
         <div class="row">
          <div class="col-sm-4">
            <div class="" style="background-color:lavender;">
            <h3>Vision</h3>
            
          <p>Our vision is to bring a circular economy into a reality where used products are manufactured into new products to minimize the over-exploitation of natural resources and maximize recycling.and and produce new usable things by using these.</p>
          </div>
      </div>
      <div class="col-sm-4">
        <div class="" style="background-color:lavender;">
        <h3>Mission</h3>
        <p>Our mission is to make a world where nothing is wasted, the communities from around the globe building an eco-system of sustainable living through doing the right waste treatment that is to get it recycled.</p>
         </div>
     </div>

         <div class="col-sm-4">
            <div class="" style="background-color:lavender;">
             <h3>Goal</h3>
             <p>Through continuous innovation of technology, our goal is to make recycling achievable and accessible to all, from institutions to individuals to achieve it in a short term of time.where the goal became achieved.</p>
            </div>
         </div>
     </div>
         </div></br>
         <section>
             <div class="container">
                <div class="row">
                    <h2 style="color:green">Our future scope</h2>
                </br>

                <div class="col-sm-9">
                    <div class="about-fth-sec">
                        <div class="about-fth-sec-ctnt">
                            <h3 style="color:green">Minimizing Landfills</h3>
                        <p><i class="fa fa-circle" aria-hidden="true"></i>

                    Landfills are spreading across India at a fast pace making it more complex each day to manage the waste management value chain.</p>
                    <p><i class="fa fa-circle" aria-hidden="true"></i>But, this can be tackled only when we start waste management from our homes in the way of waste segregation. Waste segregation is the process of differentiating dry and wet waste and disposing of it into two different bins.</p>
                    <p><i class="fa fa-circle" aria-hidden="true"></i>We get the dry segregated waste recycled as you sell your scrap to us. If we inculcate the habits of “avoiding tossing trash anywhere” and “adopting daily waste segregation”, remember we’re coming one step ahead in reducing landfills.</p>
                </br>
                 <h3 style="color:green">Maximizing Recycling</h3>
                 <p>
                    <i class="fa fa-circle" aria-hidden="true"></i>To mark a larger impact, along with serving households, The Kabadiwala is a recycling partner to small to big businesses and corporates to institutions with the aim to build a conscious ecosystem on a societal level.</p>
                    <p><i class="fa fa-circle" aria-hidden="true"></i>To accelerate recycling, The Kabadiwala manages Bhopal city’s huge garbage dumps at 4 Material Recovery Facilities commonly known as MRF centers. Where every day, more than 50 kgs of waste are segregated by our waste workers.</p>
                    <p><i class="fa fa-circle" aria-hidden="true"></i>The Kabadiwala’s campaigns with big organizations like Tetra Pak India and Indian Sleep products Federations (ISPF) based on recycling of daily utility products like Used beverage cartons and used mattresses respectively helps us maximize recycling extensively.</p>
                    
                        </div>
                        <div class="bout-fsec-imgs">
                            <img src="https://www.thekabadiwala.com/images/aboutus/about-us-waste-dump.webp">
                        </div>
                    </div>
                </div>

                 <div class="col-sm-3">
                    
                 </div>

                </div>
            </div>
         </section>
           </br>
         <section>
    <div class="container">
         <h1>The wostin brings you</h1>
         <div class="row">
          <div class="col-sm-4">
            <img src="https://www.thekabadiwala.com/images/aboutus/1.svg">
            <div class="">
            <h3>Transparency</h3>
            
          <p>Our App enables users to access the impact marked through the waste sold and recycled in terms of resources and energy saved.</p>
          </div>
      </div>
      <div class="col-sm-4">
        <img src="https://www.thekabadiwala.com/images/aboutus/2.svg">
        <div class="">
        <h3>Treceability</h3>
        <p>Our 4 different apps for users, pickup executives, city managers & MRF centers are helping us organize the unorganized waste sector in terms of keeping a track record of waste sold, segregated, bailed, transported, and recycled.</p>
         </div>
     </div>

         <div class="col-sm-4">
            <img src="https://www.thekabadiwala.com/images/aboutus/3.svg">
            <div class="">
             <h3>Scalability</h3>
             <p>Our 4 different apps for users, pickup executives, city managers & MRF centers are helping us organize the unorganized waste sector in terms of keeping a track record of waste sold, segregated, bailed, transported, and recycled.</p>
            </div>
         </div>
     </div>
         </div>
     </section>
 </br>
         <section>
             <div class="container">
                <div class="row">
                    
                </div>
             </div>
         </section>
      </br>
    
<?php get_footer(); ?>  