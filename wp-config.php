<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4{CU)o[c])e|X/uEVw`mD6YmFxY^iC|K_E,,b5;[EE2pq8`-Jj[7BuWT6+woV}8`' );
define( 'SECURE_AUTH_KEY',  '2`kI4.n-].b4JBi#ml<EI:`HbJcI8q[$VXp7N9VRPtq Q0$O=4~3bivWzXKU]Qb?' );
define( 'LOGGED_IN_KEY',    'd_u|!JUTx(I#qx9KJK9CDI2uhJgW)81s[F~#h/$0aKS^RjV#AU ;q4*)?HFzG||L' );
define( 'NONCE_KEY',        'nIB2c]<}fYO(w]H(8Q.@lk2r?OHM=,F3~elsQQ0vXY;@b--K%<d(:#`0&*by=|Wc' );
define( 'AUTH_SALT',        'I^<*iTkrVL>?wU{^yHN`8a,ZSe;+>U6L4gk$x}#~#*c3zOU1{=U.p2C3!M6x]QY{' );
define( 'SECURE_AUTH_SALT', 'uKg=K:{c=6%C-2,5.G}LqN$;WosS<kCu480mbf!Md-uB<8Z3b4S!}n?#<X)e]hLp' );
define( 'LOGGED_IN_SALT',   'grKFBlI3=|%9}K6mZ%p[O@C?.+5v+L@L<<X8}h92nz4VkD^9RmQ7?:bqi8{of.xu' );
define( 'NONCE_SALT',       'l1oqKoL%<(kjDRrN8BI=b[A}26I3Zk$C9CR{LN-p,-F|pXZ/,k8)EsI|^Sy[39g@' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
